class VisualizationsController < ApplicationController
  require 'csv'
  require 'json'

  def create
    file = params[:file]
    data = []
    headers = nil
    numbers = nil
    text = nil

    if file == nil
      flash[:error] = 'Please upload a valid CSV.'
      return redirect_to root_url
    end

    begin
      csv = CSV.foreach(file.path, headers: false) do | row |
        if headers == nil
          headers = row
          next
        end

        data.append(row)
        if numbers == nil and text == nil
          numbers = []
          text = []
          row.each_with_index do | value, index |
            if is_number?(value) then numbers.push index
            else text.push index
            end
          end
        end
      end
    rescue
      flash[:error] = 'Failed to parse file.'
      return redirect_to root_url
    end

    fields = []
    headers.each_with_index do | header, index |
      fields.push index
    end

    configs = {
      title: params[:title],
      headers: headers,
      fields: fields,
      xfields: fields,
      yfields: numbers,
      numfields: numbers,
      textfields: text
    }

    vis_params = {
      data: data.to_s,
      configs: configs.to_json
    }

    @visualization = Visualization.new(vis_params)

    respond_to do |format|
      if @visualization.save
        flash[:notice] = 'Visualization was successfully created.'
        format.html { redirect_to @visualization }
        format.json { render json: @visualization.to_hash(false), status: :created }
      else
        flash[:error] = 'Invalid CSV file format.'
        format.html { redirect_to root_url }
        format.json { render json: @visualization.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @vis = Visualization.find(params[:id])

    tf = @vis.configs['textfields']

    data = []
    eval(@vis.data).each do | row |
      arr = []
      row.each_with_index do | val, index |
        if tf.include?(index)
          logger.info val
          arr.push val.to_s
        else
          arr.push val.to_f
        end
      end
      data.push arr
    end

    respond_to do | format |
      format.html do
        logger.info data
        @data = data
        @configs = @vis.configs.to_json.html_safe

        logger.info @configs
        logger.info ActiveSupport::JSON::decode(@configs)['yfields']
      end
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end

  def is_number?(object)
    true if Float(object) rescue false
  end
end
