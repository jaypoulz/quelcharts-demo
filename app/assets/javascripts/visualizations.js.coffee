##
# visualization.js.coffee
#
$ ->
  window.qc ?= {}
  qc.curvis ?= {}

  if namespace.controller is 'visualizations'
    # Navbar Vis Switching
    $('.navbar-nav > li:first').addClass('active')
    $('.navbar-nav > li').click ->
      $(".navbar-nav > li.active").removeClass("active")
      $(this).addClass('active')

    ### Actual Vis Drawing ###
    $('#scatter_btn').click ->
      if qc.carousel? then qc.carousel.end()

      sel = []
      for key, vis of qc.curvis
        sel = sel.concat(vis.selected)
        vis.end()
        delete qc.curvis[key]

      drawScatter(sel)
      qc.curvis['scatter'] = qc.scatter

    $('#bar_btn').click ->
      if qc.carousel? then qc.carousel.end()

      sel = []
      for key, vis of qc.curvis
        sel = sel.concat(vis.selected)
        vis.end()
        delete qc.curvis[key]

      drawBar(sel)
      qc.curvis['bar'] = qc.bar

    $('#rad_btn').click ->
      if qc.carousel? then qc.carousel.end()

      sel = []
      for key, vis of qc.curvis
        sel = sel.concat(vis.selected)
        vis.end()
        delete qc.curvis[key]

      drawRadVis(sel)
      qc.curvis['rad'] = qc.radvis

    $('#hist_btn').click ->
      if qc.carousel? then qc.carousel.end()

      sel = []
      for key, vis of qc.curvis
        sel = sel.concat(vis.selected)
        vis.end()
        delete qc.curvis[key]

      drawHistogram(sel)
      qc.curvis['hist'] = qc.hist

    $('#tree_btn').click ->
      if qc.carousel? then qc.carousel.end()

      sel = []
      for key, vis of qc.curvis
        sel = sel.concat(vis.selected)
        vis.end()
        delete qc.curvis[key]

      drawTree(sel)
      qc.curvis['tree'] = qc.tree

    $('#carousel_btn').click ->
      sel = []
      for key, vis of qc.curvis
        sel = sel.concat(vis.selected)

      drawCarousel(sel)

    setSelectionListener '#vis-canvas', (o, w, h) ->
      sel = []
      for key, vis of qc.curvis
        sel = vis.onselect(o, w, h)
        if (sel.length > 0) then break

      @configs.selected = sel
      for key, vis of qc.curvis
        vis.drawChart(data, configs)

    ### QC Configs ###
    $('input[name=qc-xaxis]:radio').change (e) ->
      for key, vis of qc.curvis
        qcconfigs.setXAxisControls(vis, Number(e.target.value))

    $('input[name=qc-yaxis]:radio').change (e) ->
      for key, vis of qc.curvis
        qcconfigs.setYAxisControls(vis, [Number(e.target.value)])

    $('#carousel-rotl').click ->
      qc.carousel.rotateLeft()

    $('#carousel-rotr').click ->
      qc.carousel.rotateRight()
