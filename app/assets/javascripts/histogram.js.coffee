# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  if namespace.controller is 'visualizations' and namespace.action is 'show'
    qc.hist ?= new QCHistogram()

    window.drawHistogram = (sel) ->
      @data ?= [[200, 300], [300, 100], [400, 250], [500, 80], [600, 150]]
      @configs ?= {}
      @configs.independent ?= 0
      @configs.dependent ?= [ 1 ]
      @configs.title = 'Histogram'
      @configs.selected = sel

      qc.hist.drawChart(data, configs, true)
