# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  if namespace.controller is 'visualizations' and namespace.action is 'show'
    window.drawCarousel = (sel) ->
      @configs.selected = sel
      vises = [qc.scatter, qc.bar, qc.radvis, qc.hist, qc.tree]
      qc.curvis =
        scatter: qc.scatter
        bar: qc.bar
        radvis: qc.radvis
        hist: qc.hist
        tree: qc.tree

      qc.carousel = new QCCarousel(vises)
      qc.carousel.draw(data, configs)
