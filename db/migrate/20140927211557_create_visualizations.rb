class CreateVisualizations < ActiveRecord::Migration
  def change
    create_table :visualizations do |t|
      t.text :data, limit: nil
      t.json :configs, default: {}

      t.timestamps
    end
  end
end
